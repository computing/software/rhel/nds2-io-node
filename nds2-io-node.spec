%define release 1.1

Name:           nds2-io-node
Version:        0.1.0
Release:        %{release}%{?dist}
Summary:        nds2 IO node
BuildArch:      noarch

License:        GPL3
Source0:        https://git.ligo.org/nds/%{name}/-/archive/v%{version}/%{name}-v%{version}.tar.gz
Requires:       bash, python3-ldas-tools-framecpp, python3-netifaces, python3-numpy, python3-memcached

%description
NDS is the system used to distribute time series data from LIGO gravitation
wave data stores.  This package provides an io reader suitable for distribution
through a compute cluster or server cluster to distribute the reading of data
across a large number of systems.

%prep
%setup -q -n %{name}-v%{version}


%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT/%{_bindir}
mkdir -p $RPM_BUILD_ROOT/%{_datadir}/nds2-io-node/lib
cp debian/nds2-io-node.sh $RPM_BUILD_ROOT/%{_bindir}/nds2-io-node
chmod a+x $RPM_BUILD_ROOT/%{_bindir}/nds2-io-node
cp -r nds2-server $RPM_BUILD_ROOT/%{_datadir}/nds2-io-node/lib

%files
%{_bindir}/nds2-io-node
%{_datadir}/nds2-io-node/lib/nds2-server/tests/*.py
%{_datadir}/nds2-io-node/lib/nds2-server/reader/tests/*.py
%{_datadir}/nds2-io-node/lib/nds2-server/reader/*.py
%{_datadir}/nds2-io-node/lib/nds2-server/*.py


%changelog
* Thu Aug 18 2022 Jonathan Hanks <jhanks@caltech.edu> 0.1.0, Documentation update, version bump, no code change.
* Mon Aug 1 2022 Jonathan Hanks <jhanks@caltech.edu> 0.0.3, Close issue #7,  disk cache on the production systems sometimes gives
    inefficient frame paths.  Try to use known faster paths.
* Wed Jul 20 2022 Jonathan Hanks <jhanks@caltech.edu> 0.0.2, fixing an issue with the memcached client
* Tue Apr 12 2022 Jonathan Hanks <jhanks@caltech.edu> quick packaging

